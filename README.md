# teselecta

## Not maintained

1. Create a `token.json` in this format:

	```json
    {
      "token": "YOUR_TOKEN_HERE",
      "androidId": "",
      "masterToken": ""
    }
    ```
2. `sudo npm install`
3. `node index.js`

## License
The contents of this repository are provided under the GPLv3 license or any later version (SPDX identifier: `GPL-3.0-or-later`).
