module.exports = function (cl, conf) {
    var c = {},
		request = require('request'),
		ytdl = require('ytdl-core'),
		pm = new (require('playmusic'))(),
		yt = require('youtube-search'),
		secrets = require('../secrets'),
		voice = [],
		voiceTemplate = {
			'queue': [],
			'stream': null,
			'connection': null,
			'tc': null // text channel
		},
		services = {
			"yt": "YouTube",
			"pm": "Play Music",
			"str": "stream URL"
		};

	function check(msg, cb, joinIfNotJoined = true) {
		if(voice[msg.member.voiceChannel.id]) {
			cb(voice[msg.member.voiceChannel.id]);
		} else if(msg.member.voiceChannel && joinIfNotJoined) {
			voice[msg.member.voiceChannel.id] = voiceTemplate;
			voice[msg.member.voiceChannel.id].tc = msg.channel;
			msg.member.voiceChannel.join().then(conn => {
				voice[msg.member.voiceChannel.id].connection = conn;
				cb(voice[msg.member.voiceChannel.id]);
			}).catch(console.error);
		}
	}

	function addSong(v, service, id, friendlyName) {
		v.queue.push({"service": service, "id": id, "friendlyName": friendlyName});
		if(v.queue.length == 1) {
			playNext(v);
		} else {
			v.tc.sendMessage('`' + friendlyName + '` added to the queue').catch(console.error);
		}
	}

	function playSong(v, stream) {
		v.stream = v.connection.playStream(stream, {volume: conf.getData("/volume") / 100, passes: 3}).on('end', () => playNext(v));
		v.tc.sendMessage('Now playing: `' + v.queue[0].friendlyName + '` (' + services[v.queue[0].service] + ')').catch(console.error);
		cl.user.setGame(v.queue[0].friendlyName).catch(console.error);
		v.queue.shift();
	}

	function playNext(v) {
		if(!v.queue.length) {
			cl.user.setGame(null).catch(console.error);
		} else {
			switch (v.queue[0].service) {
				case "yt":
					playSong(v, ytdl.downloadFromInfo(v.queue[0].id, {filter: "audio"}));
					break;
				case "pm":
					pm.getStreamUrl(v.queue[0].id, (err, url) => {
						if(err) return console.error(err);
						playSong(v, request(url));
					});
					break;
				case "str":
					playSong(v, request(voice[vid].queue[0].id));
					break;
			}
		}
	}

	pm.init({androidId: secrets.androidId, masterToken: secrets.masterToken}, function(err) {if (err) console.error(err)});

	c.yt = {
		description: "add a song from YouTube to the queue",
		argdescription: "[url/id/song_name]",
		exec: function(p) {
			check(p.msg, (v) => {
				yt(p.args, {maxResults: 1, key: secrets.youtubeKey, type: "video"}, function(err, ytres) {
					if(err) return console.error(err);
					if(ytres.length == 0) {
						v.tc.sendMessage('Song not found').catch(console.error);
					} else {
						ytdl.getInfo(ytres[0].id, (err, res) => {
							addSong(v, "yt", res, res.title);
						});
					}
				});
			});
		}
	};

	c.pm = {
		description: "add a song from Google Play Music to the queue",
		argdescription: "[song_name]",
		exec: function(p) {
			check(p.msg, (v) => {
				pm.search(p.args, 50, (err, res) => {
					if(err) return console.error(err);
					if(!res.entries) {
						v.tc.sendMessage('Song not found').catch(console.error);
					} else {
						var song = res.entries.filter((val) => {return val.type == '1'})[0];
						addSong(v, "pm", song.track.nid, song.track.artist + " - " + song.track.title);
					}
				});
			});
		}
	};

	c.pmid = {
		description: "add a song from Google Play Music to the queue",
		argdescription: "[song_id]",
		exec: function(p) {
			check(p.msg, (v) => {
				pm.getAllAccessTrack(p.args, (err, res) => {
					if(res.error) {
						v.tc.sendMessage('Song not found').catch(console.error);
					} else {
						addSong(v, "pm", p.args, res.artist + " - " + res.title);
					}
				});
			});
		}
	};

	c.str = {
		description: "add a song from a stream URL to the queue",
		argdescription: "[stream_url]",
		exec: function(p) {
			check(p.msg, (v) => {
				addSong(v, "str", p.args, p.args);
			});
		}
	};

	c.p = {
		description: "pause/resume current song",
		exec: function(p) {
			check(p.msg, (v) => {
				if(v.stream) {
					if(v.stream.paused) {
						v.tc.sendMessage('Song resumed').catch(console.error);
						v.stream.resume();
					} else {
						v.tc.sendMessage('Song paused').catch(console.error);
						v.stream.pause();
					}
				}
			}, false);
		}
	};

	c.v = {
		description: "set volume",
		argdescription: "[volume_percent]",
		exec: function(p) {
			p.msg.channel.sendMessage('Volume changed to ' + p.args + '%').catch(console.error);
			try {
				conf.push("/volume", +p.args);
			} catch(err) {
				console.error(err);
			}
			
			check(p.msg, (v) => {
				if(v.stream) v.stream.setVolume(+p.args / 100);
			}, false);
		}
	};

	c.q = {
		description: "show queue",
		exec: function(p) {
			check(p.msg, (v) => {
				if(v.queue.length) {
					var queue = 'Queue:\n';
					for(var item in v.queue) {
						queue += (+item + 1) + ') `' + v.queue[item].friendlyName + '` (' + services[v.queue[item].service] + ')\n';
					}
					v.tc.sendMessage(queue).catch(console.error);
				}
			}, false);
		}
	};

	c.s = {
		description: "skip current song",
		exec: function(p) {
			check(p.msg, (v) => {
				if(v.stream) {
					v.tc.sendMessage('Song skipped').catch(console.error);
					v.stream.end();
				}
			}, false);
		}
	};

	c.e = {
		description: "clear queue and end playback",
		exec: function(p) {
			check(p.msg, (v) => {
				v.tc.sendMessage('Queue cleared and playback stopped').catch(console.error);
				v.queue = [];
				if(v.stream) v.stream.end();
				v.connection.disconnect();
				v = undefined;
			}, false);
		}
	};
	
	return c;
};
