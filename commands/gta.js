module.exports = function (cl, conf) {
    var c = {};
	
	c.daily = {
		description: "show time remaining to GTA V daily objectives reset",
		exec: function(p) {
			var now = new Date(),
				next = (now.getUTCHours() < 6 ? new Date(Date.UTC(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(), 6)) : new Date(Date.UTC(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate()+1, 6))),
				mindiff = Math.floor((next.getTime() - now.getTime()) / 60000);

			p.msg.channel.sendMessage("Daily objectives are going to be reset in " + Math.floor(mindiff / 60) + " hours" + (mindiff % 60 ? " and " + mindiff % 60 + " minutes" : "") + ".").catch(console.error);
		}
	};
	
	return c;
};
