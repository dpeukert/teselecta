module.exports = function (cl, conf) {
	var c = {};
	
	c.help = {
		description: "show this text",
		commands: true,
		exec: function(p) {
			var prefix = conf.getData("/prefix"),
			help = "";
			for(var comm in p.commands) {
				var commobj = p.commands[comm];
				help += prefix + (commobj.command || comm) + (commobj.argdescription ? " " + commobj.argdescription : "") + " - " + commobj.description + "\n";
			}
			p.msg.channel.sendMessage(help).catch(console.error);
		}
	};

	c.info = {
		description: "show basic info about this bot",
		exec: function(p) {
			var discordjsver = "https://github.com/hydrabolt/discord.js/tree/"+require("../node_modules/discord.js/package.json").gitHead;
			var ytdlcorever = require("../package.json").dependencies["ytdl-core"];
			var ytsearchver = require("../package.json").dependencies["youtube-search"];
			var playmusicver = require("../package.json").dependencies["playmusic"];
			p.msg.channel.sendMessage("**Teselecta** by <@145566691630252032>\n\nGitHub repository: https://github.com/dpeukert/teselecta\n\n`"+conf.getData("/prefix")+"help` to show all available commands\n\nConnected to "+cl.guilds.size+" servers, "+cl.channels.size+" text channels and "+cl.voice.connections.size+" voice channels\n\n__Versions:__\n**discord.js**             "+discordjsver+"\n**ytdl-core**              "+ytdlcorever+"\n**youtube-search**  "+ytsearchver+"\n**playmusic**            "+playmusicver).catch(console.error);
		}
	};

	c.d = {
		description: "delete messages from this bot",
		exec: function(p) {
			var prefix = conf.getData("/prefix");
			p.msg.channel.fetchMessages({limit: 100}).then(messages => {
				messages.filter((val) => {return (val.author.id == cl.user.id ? true : false)}).deleteAll();
			}).catch(console.error);
		}
	};

	c.da = {
		description: "delete all messages in a channel",
		exec: function(p) {
			p.msg.channel.fetchMessages({limit: 100}).then(messages => {
				messages.deleteAll();
			}).catch(console.error);
		}
	};

	c.pr = {
		description: "change prefix for commands",
		argdescription: "[prefix]",
		exec: function(p) {
			try {
				conf.push("/prefix", p.args);
			} catch(err) {
				console.error(err);
			}
			p.msg.channel.sendMessage("Prefix changed").catch(console.error);
		}
	};

	return c;
};
