var fs = require('fs'),
	discord = require("discord.js"),
	jsondb = require("node-json-db"),
	merge = require("merge"),
	cl = new discord.Client(),
	conf = new jsondb("config",true,true),
	commandfiles = fs.readdirSync("./commands");

for(var file in commandfiles) commandfiles[file] = require("./commands/" + commandfiles[file])(cl, conf);
var commands = merge(...commandfiles);

cl.on('ready', () => {
 	console.log("Bot ready!");
});

cl.on('disconnect', () => {
	console.log("Bot disconnected!");
	process.exit(1);
});

cl.on('message', (msg) => {
	var prefix = conf.getData("/prefix");

	if(msg.content.indexOf(prefix) != 0) return;
	if(msg.author.id == cl.user.id) return;

	msg.content = msg.content.substr(prefix.length);

	var messageparts = (msg.content.indexOf(" ") != -1 ? [msg.content.slice(0,msg.content.indexOf(" ")), msg.content.slice(msg.content.indexOf(" ")+1)] : [msg.content, null]),
		command = messageparts[0].toLowerCase(),
		args = messageparts[1];

	for(var comm in commands) {
		var commobj = commands[comm],
			commname = commobj.command || comm;

		if(command == commname) {
			msg.delete().catch(console.error);
			var params = {cl: cl, msg: msg, args: args};
			if(args) params['args'] = args;
			if(commobj.commands) params['commands'] = commands;
			commobj.exec(params);
			break;
		}
	}
});


cl.login(require("./secrets").token);
